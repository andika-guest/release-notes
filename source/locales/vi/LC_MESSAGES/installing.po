# Vietnamese translation for Release Notes (Installing).
# Copyright © 2009 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2009-02-13 17:39+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language: vi\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Plural-Forms: nplurals=1; plural=0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../installing.rst:4
msgid "Installation System"
msgstr "Hệ thống Cài đặt"

#: ../installing.rst:6
msgid ""
"The Debian Installer is the official installation system for Debian. It "
"offers a variety of installation methods. The methods that are available "
"to install your system depend on its architecture."
msgstr ""
"Bản Cài đặt Debian là hệ thống cài đặt chính thức của Debian. Nó cung cấp"
" vài phương pháp cài đặt khác nhau. Những phương pháp sẵn sàng cài đặt "
"vào hệ thống của người dùng thì phụ thuộc vào kiến trúc."

#: ../installing.rst:10
#, fuzzy
msgid ""
"Images of the installer for |RELEASENAME| can be found together with the "
"Installation Guide on the Debian website (|URL-INSTALLER|)."
msgstr ""
"Các ảnh chứa bản cài đặt cho |RELEASENAME| nằm cùng với Sổ tay Cài đặt ở "
"địa chỉ Web của Debian (|URL-INSTALLER|)."

# | msgid ""
# | "The Installation Guide is also included on the first CD/DVD of the "
# | "official Debian CD/DVD sets, at:"
#: ../installing.rst:13
#, fuzzy
msgid ""
"The Installation Guide is also included on the first media of the "
"official Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Sổ tay Cài đặt cũng có sẵn trên đĩa CD/DVD thứ nhất của bộ đĩa Debian "
"chính thức, ở :"

#: ../installing.rst:20
#, fuzzy
msgid ""
"You may also want to check the errata for debian-installer at |URL-"
"INSTALLER-ERRATA| for a list of known issues."
msgstr ""
"Khuyên người dùng cũng đọc danh sách <ulink url=\"&url-"
"installer;index#errata\">errata</ulink> tìm danh sách các vấn đề được "
"biết."

#: ../installing.rst:26
msgid "What's new in the installation system?"
msgstr "Hệ thống cài đặt có gì mới?"

# | msgid ""
# | "There has been a lot of development on the Debian Installer since its "
# | "first official release with Debian 3.1 (sarge)  resulting in both "
# | "improved hardware support and some exciting new features."
#: ../installing.rst:28
#, fuzzy
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with Debian |OLDRELEASE|, resulting in improved"
" hardware support and some exciting new features or improvements."
msgstr ""
"Có rất nhiều phát triển trong Bản Cài Đặt Debian kể từ lần phát hành "
"chính thức đầu tiên với Debian 3.1 (sarge), kết quả là cả hai cải tiến "
"hỗ trợ phần cứng và một số tính năng mới để kích thích."

#: ../installing.rst:32
msgid ""
"If you are interested in an overview of the changes since "
"|OLDRELEASENAME|, please check the release announcements for the "
"|RELEASENAME| beta and RC releases available from the Debian Installer's "
"`news history <https://www.debian.org/devel/debian-installer/News/>`__."
msgstr ""
"Trong Ghi chú Phát hành này chỉ liệt kê những thay đổi chính trong bản cài "
"đặt. Nếu người dùng quan tâm đến toàn cảnh về những thay đổi chi tiết kể từ "
"|OLDRELEASENAME|, hãy kiểm tra các thông cáo phát hành đối với những bản "
"phát hành B và RC của |RELEASENAME| mà sẵn sàng từ trang Web `lịch sử tin tức "
"<https://www.debian.org/devel/debian-installer/News/>`__ của Bản Cài Đặt Debian."

#: ../installing.rst:41
msgid "Something"
msgstr ""

#: ../installing.rst:43
msgid "Text"
msgstr ""

# | msgid "Automated installation"
#: ../installing.rst:48
#, fuzzy
msgid "Cloud installations"
msgstr "Tự động cài đặt"

#: ../installing.rst:50
msgid ""
"The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian"
" |RELEASENAME| for several popular cloud computing services including:"
msgstr ""

#: ../installing.rst:53
msgid "Amazon Web Services"
msgstr ""

#: ../installing.rst:55
msgid "Microsoft Azure"
msgstr ""

#: ../installing.rst:57
msgid "OpenStack"
msgstr ""

#: ../installing.rst:59
msgid "Plain VM"
msgstr ""

#: ../installing.rst:61
msgid ""
"Cloud images provide automation hooks via ``cloud-init`` and prioritize "
"fast instance startup using specifically optimized kernel packages and "
"grub configurations. Images supporting different architectures are "
"provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""

#: ../installing.rst:67
msgid ""
"The cloud team will provide updated images until the end of the LTS "
"period for |RELEASENAME|. New images are typically released for each "
"point release and after security fixes for critical packages. The cloud "
"team's full support policy can be found `here "
"<https://wiki.debian.org/Cloud/ImageLifecycle>`__."
msgstr ""

#: ../installing.rst:73
msgid ""
"More details are available at `<https://cloud.debian.org/>`__ and `on the"
" wiki <https://wiki.debian.org/Cloud/>`__."
msgstr ""

#: ../installing.rst:79
msgid "Container and Virtual Machine images"
msgstr ""

#: ../installing.rst:81
msgid ""
"Multi-architecture Debian |RELEASENAME| container images are available on"
" `Docker Hub <https://hub.docker.com/_/debian>`__. In addition to the "
"standard images, a \"slim\" variant is available that reduces disk usage."
msgstr ""

#: ../installing.rst:86
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published"
" to `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
msgstr ""
