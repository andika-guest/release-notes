Dear translator(s),

Thanks for considering the translation of this document into your language.

If you are going to update a translation, in order to ensure that nobody is
currently working already on a translation of this document and no efforts are
duplicated, please contact the following persons to notify your intent:

 a) the previous translator(s) if they exist,
 b) the translation team for your language.

To find who to contact in a), please review the translator's list at the end of
this document. This list includes all the previous translators.

For b), you should use the translation team for your language. Translation
teams usually use distribution lists which are named as:
debian-l10n-<language>@lists.debian.org (replace <language> with the
language name such as "spanish", for Spanish).  You can find a good list of such
team at https://lists.debian.org/i18n.html .

If no translation team exists yet for your language, you might consider setting
up an official list to coordinate translations.

If one exists, please note that translation teams usually have their rules and
styles related to the translation (to maintain consistency across all
translations being produced), you should review the existing rules and
try to adhere to these too.

When contacting people, please give a few weeks before calling anyone
non-responsive. The Debian BTS for the release-notes
(https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=release-notes)
and its mailing list (debian-doc@lists.debian.org) are also other good
backup contact points in case you don't obtain answers to your initial emails.


-- Sun, 19 Dec 2010 14:02:54 +0100

Javier Fernandez-Sanguino <jfs@debian.org>


=============================================================================

Technical notes for translators:
--------------------------------

This document is available in many languages.

To work on translations, you have to use the PO based translation system.



Update an existing translation
------------------------------

 * Make sure to have the build dependencies installed:
 $ sudo apt-get install latexmk python3-distro-info python3-sphinx python3-stemmer python3-sphinx-rtd-theme tex-gyre texinfo texlive-fonts-recommended texlive-lang-all texlive-latex-extra texlive-latex-recommended git

 * To update your po files (here for German) against the English original, use:
 $ make update-po LANGS=de
 $ rm source/locales/*.pot

 * Now you can work on your po files, to update your translation.

 * Test building html|txt|pdf files from the PO files as follows:
 $ make html LANGS=de
 $ make txt LANGS=de
 $ make pdf LANGS=de
 * Now you will find different document formats for your translation under build/.
 * Make sure to proof-read and correct.

 * Then commit your files to git:
 $ git add de/LC_MESSAGES/*.po
 $ git commit -m "Updated german translation for release-notes"
 $ git push



Create a new translation
------------------------

For example, let's assume "uk" translation is new. Here
is a rough outline of what steps to take in order to create it:

 * Obtain source files as described in https://www.debian.org/doc/vcs#access:
 $ git clone git@salsa.debian.org:ddp-team/release-notes.git
 $ cd release-notes
 $ mkdir uk

 * Make sure to have the build dependencies installed:
 $ sudo apt install latexmk python3-distro-info python3-sphinx python3-stemmer tex-gyre texinfo texlive-fonts-recommended texlive-lang-all texlive-latex-extra texlive-latex-recommended git

 * To create initial po files and update them against the English original, use:
 $ make update-po LANGS=uk
 $ rm source/locales/*.pot

 * Use your favourite PO editor to translate all the po files you will
   find under your language directory ('source/locales/uk/LC_MESSAGES' in our example).
   You may wish to edit its header manually to be perfect.

 * Test building html files from the PO files as follows:
 $ make html LANGS=uk
 * Or build all formats:
 $ make LANGS=uk
 * Now you will find different document formats for your translation under build/.
 * Make sure to proof-read and correct.

 * Then commit your files to git:
 $ git add source/locales/uk/LC_MESSAGES/*.po
 $ git commit -m "Add new language to release-notes: ukrainian"
 $ git push

All translations have to use PO files. So you can commit unfinished
translations to the repository as soon as they compile.

When you commit to the git repository, please make sure to commit your PO files
only and keep all other files such as rst files untouched.

If you cannot commit files yourself, please send your translation for inclusion
to the public mailing list debian-doc@lists.debian.org.

=============================================================================

Translators:
-----------

This list (ordered alphabetically by language code) is just a place holder 
for information introduced by the maintainer of this document but it still 
is a good starting point for people to identify possible contacts for 
translators.  In addition to this list you should also review and use the 
latest information from the header of PO files as well as any README files 
that might be present in the translation sub-directory:

 - Catalan ('ca')

        Translator(s): Hèctor Oron Martínez <zumbi@debian.org> [Coordinator]
        Language Team: Catalan <debian-l10n-catalan@lists.debian.org>

        Past Translator(s): Miguel Gea Milvaques

 - Czech ('cs')

        Translator(s): Miroslav Kure <kurem@debian.cz>
        Language Team: Czech <debian-l10n-czech@lists.debian.org>

 - German ('de')

        Translator(s): 
                Holger Wansing <hwansing@mailbox.org> [ Coordinator ]
		Helge Kreutzmann <debian@helgefjell.de>

	Old Translator(s):
		Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>
                Tobias Quathamer <toddy@debian.org>
                Chris Leick <c.leick@vollbio.de>
                Jan Hauke Rahm <jhr@debian.org>
	        Jens Seidel
                Dominink Grotegerd <dgrotegerd@web.de>

        Language Team: German <debian-l10n-german@lists.debian.org>

 - Spanish ('es')

        Translator(s):
                  Javier Fernandez-Sanguino <jfs@debian.org> [Coordinator] 
                  Francisco Javier Cuadrado <fcocuadrado@gmail.com> 
                  Fernando González de Requena <fgrequena@gmail.org>
                  Ricardo Cardenes Medina
                  Juan Manuel Garcia Molina
                  David Martínez Moreno

        Language Team:  Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>

 - Finnish ('fi')

        Translator(s):
            Tapio Lehtonen <tale@debian.org> [Coordinator?]
            Tommi Vainikainen <thv@iki.fi>

        Language Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"

 - French ('fr')

        Translator(s): 
            Christian Perrier <bubulle@debian.org> [ Coordinator ]
            Thomas Vincent <thomas@vinc-net.fr>
            Thomas Blein <tblein@tblein.eu>
            Romain Doumenc <romain.doumenc@gmail.com>
            Steve R. Petruzzello <dlist@bluewin.ch>
            David Prévot <david@tilapin.org>
            Simon Paillard <simon.paillard@resel.enst-bretagne.fr>
            Jean-Luc Coulon <jean-luc.coulon@wanadoo.fr>
            Philippe Batailler <philippe.batailler@free.fr>
            Stéphane Blondon <stephane.blondon@gmail.com>
            Yannick Roehlly <yannick.roehlly@free.fr>
            Frédéric Bothamy <frederic.bothamy@free.fr>
            Denis Barbier <bouzim@gmail.com>

        Language Team:  French <debian-l10n-french@lists.debian.org>
        
 - Italian ('it')

        Translator(s):
           Luca Monducci <luca.mo@tiscali.it> [ Coordinator? ]
           Luca Brivio <lucab83@infinito.it>
           Vincenzo Campanella <vinz65@gmail.com>
           Beatrice Torracca <beatricet@libero.it>
           Gabriele Stilli <superenzima@libero.it>

        Language Team: Debian Italian Team <debian-l10n-italian@lists.debian.org>

 - Japanese ('ja')

        Translator(s):
           KURASAWA Nozomu <nabetaro@caldron.jp> [ Coordinator? ] 
           Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>
           Satoru Kurashiki
           Nobuhiro IMAI
           Hisashi MORITA

        Language Team:  Japanese <debian-doc@debian.or.jp>

 - Lithuanian ('lt')

        Translator(s): 
            Kęstutis Biliūnas <kebil@kaunas.init.lt>
            Mantas Kriaučiūnas <mantas@akl.lt>
            Vytautas Paltanavičius <vytas@atviras.lt>

        Language Team: Lithuanian <komp_lt@konferencijos.lt>

 - Malayalam ('ml')

        Translator(s):
            Praveen Arimbrathodiyil <pravi.a@gmail.com> [ Coordinator? ]
            Praveen P <pravin.vet@gmail.com>
            Sankaranarayanan <snalledam@dataone.in>
            Syam Krishnan <syamcr@gmail.com>
            Manilal K M <libregeek@gmail.com>

        Language Team: Malayalam <smc-discuss@googlegroups.com>

 - Norwegian ('nb')

        Translator(s): 
            Klaus Ade Johnstad <klaus@skolelinux.no>
        Language Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>

 - Dutch ('nl')

        Translator(s):
            Jeroen Schot

        Language Team: Debian l10n Dutch <debian-l10n-dutch@lists.debian.org>

        Past Translator(s):
            Eric Spreen
            Remco Rijnders
        
 - Portuguese ('pt')

        Translator(s):
             Miguel Figueiredo <elmig@debianpt.org> [ Coordinator? ]
             Rui Branco <ruipb@debianpt.org>
             Ricardo Silva <ardoric@gmail.com>
             Carlos Lisboa <carloslisboa@gmail.com>
             Pedro Ribeiro <p.m42.ribeiro@gmail.com>
             António Moreira <antoniocostamoreira@gmail.com>
             Américo Monteiro <a_monteiro@netcabo.pt>

        Language Team: Portuguese <traduz@debianpt.org>

 - Brazilian Portuguese ('pt_BR')

        Translator(s): 
            Felipe Augusto van de Wiel <faw@debian.org> [Coordinator]
            Marcelo Gomes de Santana <marcelo@msantana.eng.br>

        Language Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian.org>

 - Romanian ('ro')

        Translator(s):
            Andrei Popescu <andreimpopescu@gmail.com> [ Coordinator? ]
            Igor Știrbu <igor.stirbu@gmail.com>
            Eddy Petrișor <eddy.petrisor@gmail.com>
            Stan Ioan-Eugen <stan.ieugen@gmail.com>
            Dan Damian <dand@codemonkey.ro>
            Vitalie Lazu <vitalie.lazu@gmail.com>
            Sorin-Mihai Vârgolici <smv@yobicore.org>

        Language Team: Romanian Team <debian-l10n-romanian@lists.debian.org>

 - Russian ('ru')

        Translator(s):
            Yuri Kozlov <yuray@komyakino.ru> | <kozlov.y@gmail.com> [Coordinator?]
            Sergey Alyoshin <alyoshin.s@gmail.com>

        Language Team: Russian <debian-l10n-russian@lists.debian.org>\n"

 - Slovak ('sk')

        Translator(s): Ivan Masár <helix84@centrum.sk>
        Language Team: Slovak <sk-i18n@lists.linux.sk>

 - Swedish ('sv')

        Translator(s): Martin Bagge <brother@bsnet.se>

        Language Team: Swedish <debian-l10n-swedish@lists.debian.org>

 - Vietnamese ('vi')

        Translator(s): Clytie Siddall <clytie@riverland.net.au>

        Language Team: Vietnamese <vi-VN@googlegroups.com>

 - Chinese - Simplified ('zh_CN')

        Translator(s):
            Dongsheng Song <dongsheng.song@gmail.com> [Coordinator?]
            Ji ZhengYu <zhengyuji@gmail.com> 
            chenxianren <chenxianren@gmail.com>
            Deng Xiyue <manphiz@gmail.com>
            LI Daobing <lidaobing@gmail.com>
            R. N. Engch <niatlantice@gmail.com>
            Lave <lave.wang.w@gmail.com>

        Language Team: Chinese (Simplified & others) <debian-l10n-chinese@lists.debian.org>

 - Chinese - Traditional ('zh_TW')

        Translator(s): Tetralet <tetralet@gmail.com>
        Language Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists.debian.org>
